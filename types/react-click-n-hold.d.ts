export default class _default {
    constructor(props: any);
    clickCapture(e: any): void;
    componentWillUnmount(): void;
    end(e: any): any;
    forceUpdate(callback: any): void;
    render(): any;
    setState(partialState: any, callback: any): void;
    start(e: any): void;
    timeout(start: any): void;
}
