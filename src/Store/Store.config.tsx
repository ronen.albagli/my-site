import { createStore, combineReducers, applyMiddleware, compose } from 'redux';
import thunk from 'redux-thunk';
import MainReducer from './Reducers/MainReducer';
import ElementReducer from './Reducers/WebBuilderReducers/ElementReducer';

const composeEnhacers = (window as any).__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

export default () => {
    const store = createStore(
        combineReducers({
            Main: MainReducer,
            WebBuilder  : ElementReducer,
        }),
        composeEnhacers(applyMiddleware(thunk))
    )
    return store;
}