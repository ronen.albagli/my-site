import { ElementActionTypes } from '../../../Types/WebBuilderTypes';

export const AddElementToDOM = (type: string = 'text') => ({
    type: ElementActionTypes.ADD_ELEMENT_TO_DOM,
    payload: type
})