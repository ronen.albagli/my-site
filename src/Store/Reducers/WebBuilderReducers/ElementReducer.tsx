import { ElementActionTypes } from '../../../Types/WebBuilderTypes';
import { makeId } from '../../../components/Projects/WebBuilder/Utils/FakeID';

let elementCounter = 0;
const elementArray: Array<object> = [];

export default (state: any = [], action: any) => {
    switch (action.type) {
        case ElementActionTypes.ADD_ELEMENT_TO_DOM:
            elementCounter++;
            elementArray.push({ id: makeId(), classId: makeId(), type: action.payload });
            return [...elementArray];
        default:
            return { ...state };
    }
}