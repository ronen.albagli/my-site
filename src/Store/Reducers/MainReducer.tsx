export default (state = [], action: any) => {
    switch (action.type) {
        case 'TEST':
            return { ...state, ...action.payload }
        default:
            return { ...state }
    }
}