import * as React from 'react';
import Styled from 'styled-components';
import ProjectLists from './ProjectList/ProjectsList';

class HomeComponent extends React.Component {



    render() {
        return (
            <HomeContainer>
                <Cover>
                    <HomeTitles>
                        <h1>Welcome To Ronen Albagli WebSite </h1>
                        <h3>Here is my You can take a look on some of my tiny Projects That has been made for fun! Enjoy!</h3>
                    </HomeTitles>
                    <ProjectLists />
                </Cover>
            </HomeContainer>
        )
    }

}

const HomeContainer = Styled.div`

    position: absolute;
    top:0;
    right:0;
    left: 0;
    bottom: 0;
    // background-color: #000000b8;
    // color: white;
    // background: repeating-linear-gradient(-45deg, transparent, gray 1px, transparent 5px, black 1px);
`

const Cover = Styled.div`
    // background: linear-gradient(217deg,rgba(106,35,35,0.8),rgba(255,0,0,0) 70.71%),
    // linear-gradient(127deg,rgba(0,0,0,0.8),rgba(0,255,0,0) 70.71%),
    // linear-gradient(336deg,rgba(4,4,46,0.8),rgba(0,0,255,0) 70.71%);
    position: absolute;
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;
    padding: 15px 10px;
    display: flex;
    flex-direction: column
    align-items: center;
`
const HomeTitles = Styled.div`
padding: 15px 10px;
display: flex;
flex-direction: column
align-items: center;
`
export default HomeComponent;