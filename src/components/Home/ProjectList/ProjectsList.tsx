import * as React from 'react';
import Styled from 'styled-components';

const ProjectLists = () => {

    const tempArr = [1, 2, 3, 4, 5];


    return (
        <ProjectContanier>
            {tempArr.map((proj, index) => (
                <ProjectBox key={index}>
                    Proj ## {index}
                </ProjectBox>))
            }
        </ProjectContanier>
    )
}

const ProjectContanier = Styled.div`
    display:flex;
    align-items: center;
    justify-content: space-between;
    width: 100%;
    flex-wrap: wrap;
`

const ProjectBox = Styled.div`
    height: 300px;
    width: 32%;
    border: 1px solid white;
    margin: 5px 0px;
    box-shadow: 1px 1px 1px 1px #bbb;
`

export default ProjectLists;