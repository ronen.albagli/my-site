import * as React from 'react';
import SideBar from './SideBar/SideBar';
import BuilderView from './BuilderView/BuilderView';
import Styled from 'styled-components';

class WebBuilder extends React.Component {


    render() {
        return (
            <div>
                <SideBarContainer>
                    <SideBar />
                </SideBarContainer>
                <BuilderView />
            </div>
        )
    }
}

const SideBarContainer = Styled.div`
    position: relative;
`;

export default WebBuilder;