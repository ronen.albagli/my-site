export function drag_start(event, id) {
    var style = window.getComputedStyle(id, null);
    document.getElementById(id).dataTransfer.setData("text/plain",
        (parseInt(style.getPropertyValue("left"), 10) - event.clientX) + ',' + (parseInt(style.getPropertyValue("top"), 10) - event.clientY));
}
export function drag_over(event,id) {
    document.getElementById(id).preventDefault();
    return false;
}
export function drop(event, id) {
    var offset = event.dataTransfer.getData("text/plain").split(',');
    var dm = document.getElementById(id);
    dm.style.left = (event.clientX + parseInt(offset[0], 10)) + 'px';
    dm.style.top = (event.clientY + parseInt(offset[1], 10)) + 'px';
    event.preventDefault();
    return false;
}

