import * as React from 'react';
import Styled from 'styled-components';
import './Elements.css';
import { makeResizableDiv } from '../../Utils/ResizeElements';
import { drag_start, drag_over, drop } from '../../Utils/DragNDrop';
class WebBuilderElement extends React.Component<any, any> {
    constructor(props: any) {
        super(props);
        this.state = {
            enableToDrag: false,
        }
    }

    render() {
        const { ID, classId, isActive } = this.props;
        if (this.props.isActive) {
            makeResizableDiv(`.${classId}`)
            const test = document.getElementById(ID)
            const dm = document.getElementById(classId);
            test!.addEventListener('mousedown', () => {
                this.setState(() => ({ enableToDrag: true }))
                dm!.removeEventListener('dragstart', (e) => drag_start(e, ID), false);
                document.body.removeEventListener('dragover', drag_over, false);
                document.body.removeEventListener('drop', (e) => drop(e, classId), false);
            })
            test!.addEventListener('mouseup', () => {
                this.setState(() => ({ enableToDrag: false }))
                dm!.addEventListener('dragstart', (e) => drag_start(e,ID), false);
                document.body.addEventListener('dragover', (e) => drag_over(e, ID), false);
                document.body.addEventListener('drop', (e) => drop(e, classId), false);
            })




        }
        console.log('elemen props', this.props)
        return (
            <div onClick={() => this.props.setActive(classId)} draggable id={classId} style={{ position: 'absolute' }}>
                <div className={`resizable ${classId} ${isActive ? 'activeElement' : 'enactiveElement'}`}  >
                    <div className='resizers'>

                        {
                            this.props.isVideo ?
                                <iframe width="100%" height="100%"
                                    src="https://www.youtube.com/embed/tgbNymZ7vqY">
                                </iframe>
                                : <span>Texxt</span>
                        }
                        <div className={`${isActive ? 'resizer' : ''} top-left ${isActive ? 'activeElement' : 'enactiveElement'}  `}></div>
                        <div className={`${isActive ? 'resizer' : ''} top-middle ${isActive ? 'activeElement' : 'enactiveElement'}  `} id={ID}></div>
                        <div className={`${isActive ? 'resizer' : ''}  top-right ${isActive ? 'activeElement' : 'enactiveElement'}`}></div>
                        <div className={`${isActive ? 'resizer' : ''}  bottom-left ${isActive ? 'activeElement' : 'enactiveElement'}`}></div>
                        <div className={`${isActive ? 'resizer' : ''}  bottom-right ${isActive ? 'activeElement' : 'enactiveElement'}`}></div>
                    </div>
                </div>
            </div>
        )
    }
}

export default WebBuilderElement;