import * as React from 'react';
import Styled from 'styled-components';
import { connect } from 'react-redux';
import WebBuilderElement from './Elements/Element';
import styled from 'styled-components';

class BuilderView extends React.Component<any, any> {
    constructor(props: any) {
        super(props);
        this.state = {
            activeElement: '',
        }
    }

    setActiveElement = (elementId: string) => {
        this.setState(({ activeElement: elementId }));
    }

    render() {
        return (
            <BuilderContanier>
                {
                    this.props.WebBuilderElements.length && this.props.WebBuilderElements.map((element: any, index: number) => {
                        return (
                            <WebBuilderElement key={index} classId={element.classId} ID={element.id} setActive={this.setActiveElement} 
                            isActive={this.state.activeElement === element.classId} isVideo={element.type === 'video'}></WebBuilderElement>
                        )
                    })
                }
                <Test></Test>



            </BuilderContanier>
        )
    }
}

const mapStateToProps = (state: any) => {
    console.log('state', state);
    return ({
        WebBuilderElements: state.WebBuilder
    })
}

export default connect(mapStateToProps)(BuilderView);


const BuilderContanier = Styled.div`
    width: 100%;
    height: 100%;
    /* position: absolute;
    left: 22%;
    top: 5% */
`;

const Test = styled.div`
    height: 777px;
    width: 78%;
    left: 21%;
    position: relative;
    top: 5px;
    box-shadow: 1px 1px 1px 1px #bbb;

`;