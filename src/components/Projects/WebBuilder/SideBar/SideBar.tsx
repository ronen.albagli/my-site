import * as React from 'react';
import Styled from 'styled-components';
import styled from 'styled-components';
import './SideBar.css';
import { connect } from 'react-redux';
import { AddElementToDOM } from '../../../../Store/Actions/WebBuilderActions/ElementAction';

class SideBar extends React.Component<any, { activeTab: string }> {
    constructor(props: any) {
        super(props);
        this.state = {
            activeTab: 'text'
        }
    }
    changeActiveTab = (tabName: string) => {
        this.setState(({ activeTab: tabName }))
        if(tabName === 'videos'){
            this.props.dispatch(AddElementToDOM('video'))
        }
    }

    addElement = () => {
        this.props.dispatch(AddElementToDOM())
    }

    render() {
        return (
            <SideBarContainer>
                <ToolBarContainer >
                    <div className={` tab ${this.state.activeTab === 'text' ? 'activeTab' : ''}`} onClick={() => this.changeActiveTab('text')} >Text</div>
                    <div className={` tab ${this.state.activeTab === 'image' ? 'activeTab' : ''}`} onClick={() => this.changeActiveTab('image')}>Image</div>
                    <div className={` tab ${this.state.activeTab === 'font' ? 'activeTab' : ''}`} onClick={() => this.changeActiveTab('font')}>Font</div>
                    <div className={` tab ${this.state.activeTab === 'videos' ? 'activeTab' : ''}`} onClick={() => this.changeActiveTab('videos')}>Videos</div>
                </ToolBarContainer>
                <AdditionalButtonContainer onClick={this.addElement}>
                    <div>+</div>
                </AdditionalButtonContainer>
            </SideBarContainer>
        )
    }

}

// const mapStateToProps = (state: any) => {
//     return ({
//         WebBuilder: 'test'
//     })
// }

export default connect()(SideBar);


const SideBarContainer = Styled.div`
    position: fixed;
    height: 100%;
    top: 0;
    left: 0;
    right: 80%;
    bottom: 0;
    background-color: #000000c9;
`

const ToolBarContainer = styled.div`
    display: flex;
    flex-direction: row;
    color: white;
    justify-content: space-evenly;
    font-size: 13px;
    font-weight: bold;
    margin-top: 20px;
    height:25px;
    align-items: center;
`

const AdditionalButtonContainer = Styled.div`
position: absolute;
bottom: 5%;
right: 5%;
border: 1px solid white;
height: 35px;
width: 35px;
display: flex;
align-items: center;
justify-content: center;
border-radius: 50px;
color: white;
font-weight: 900;
`