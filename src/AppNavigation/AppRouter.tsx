import React from 'react';
import { Router, Route, Switch, Link, NavLink } from 'react-router-dom';
import createHistory from 'history/createBrowserHistory';
import { connect } from 'react-redux';
import HomeComponent from '../components/Home/Home';
import WebBuilder from '../components/Projects/WebBuilder/WebBuilder';

export const history = createHistory();

const AppRouter = (props: any) => {
    return (
        <Router history={history}>
            <div>
                <Switch>
                    <Route path="/" component={HomeComponent} exact={true} />
                    <Route path="/web-builder" component={WebBuilder} />

                </Switch>
            </div>
        </Router>
    );
}

// const mapStateTpProps = (state, props) => {
//     return ({
//         props: { ...state }
//     })
// }
export default connect()(AppRouter);
